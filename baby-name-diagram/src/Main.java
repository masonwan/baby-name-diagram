import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    //    static File nameFile = new File("names.txt");
    static File nameFile = new File("names-short.txt");
    static String targetName = "aabriella";
    static String targetGender = "f";

    public static void main(String[] args) throws FileNotFoundException {
        checkNameFile();
//        getName();
        initializeDiagram();
        generateDiagram();
    }

    static void checkNameFile() {
        if (!nameFile.exists()) {
            System.out.println("The name file does not exist.");
            System.exit(1);
        } else if (!nameFile.canRead()) {
            System.out.println("Cannot read the name file.");
            System.exit(1);
        }
    }

    static void getName() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.println("Type a name and gender");
                targetName = scanner.next().toLowerCase();
                targetGender = scanner.next().toLowerCase();

                boolean isOkay = false;
                if (targetGender.equals("m") || targetGender.equals("f")) {
                    isOkay = true;
                }

                if (isOkay) {
                    break;
                }

                System.out.println("The gender should be either 'm' or 'f'.\n");
            }
        }
    }

    static void initializeDiagram() {
        int upperGapDistance = 30, bottomGapDistance = 30;
        int numRows = 10, numColumns = 13;
        int numRowPixels = 50, numColPixels = 50;
        int maxX = numColumns * numColPixels, maxY = numRows * numRowPixels + upperGapDistance + bottomGapDistance;

        DrawingPanel drawingPanel = new DrawingPanel(maxX + 100, maxY + 100);
        Graphics graphics = drawingPanel.getGraphics();
        graphics.setFont(new Font("Consolas", Font.BOLD, 20));

        // Draw upper and bottom sections.
        graphics.setColor(Color.YELLOW);
        graphics.fillRect(0, 0, maxX, upperGapDistance);
        graphics.fillRect(0, maxY - bottomGapDistance, maxX, bottomGapDistance);

        // Drawing horizontal lines.
        int x = 0, y = 0;
        graphics.setColor(Color.GRAY);
        graphics.drawLine(0, y, maxX, y);
        y = upperGapDistance;
        graphics.drawLine(0, y, maxX, y);
        for (int i = 0; i < numRows; i++) {
            y += numColPixels;
            graphics.drawLine(0, y, maxX, y);
        }
        y = y + bottomGapDistance;
        graphics.drawLine(0, y, maxX, y);

        // Drawing vertical lines.
        x = 0;
        graphics.drawLine(x, 0, x, maxY);
        int start = upperGapDistance, end = maxY - bottomGapDistance;
        for (int i = 0; i < numColumns; i++) {
            x += numRowPixels;
            graphics.drawLine(x, start, x, end);
        }

        // Draw horizontal legend.
        graphics.setColor(Color.BLACK);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 12));
        x = 0;
        y = maxY - bottomGapDistance + 12;
        for (int i = 0, year = 1880; i < numColumns; i++, year += 10) {
            graphics.drawString(String.valueOf(year), x, y);
            x += numRowPixels;
        }
    }

    static void generateDiagram() throws FileNotFoundException {
        boolean isFound = false;

        try (Scanner scanner = new Scanner(nameFile)) {
            isFound = false;
            while (scanner.hasNext()) {
                String name = scanner.next().toLowerCase();
                String data = scanner.nextLine();

                if (name.equals(targetName)) {
                    isFound = true;

                    break;
                }
            }
        }

        if (!isFound) {
            System.out.printf("\"%s\" (%s) not found\n", toSentenceCase(targetName), targetGender.toUpperCase());
        }
    }

    static String toSentenceCase(String name) {
        char c = Character.toUpperCase(name.charAt(0));
        name = name.substring(1);
        return c + name;
    }

    /**
     * Generate a shorter version of name file for testing.
     *
     * @throws FileNotFoundException
     */
    static void generateShortNameFile() throws FileNotFoundException {
        try (Scanner scanner = new Scanner(nameFile)) {
            int max = 30;
            for (int i = 0; i < max; i++) {
                String line = scanner.nextLine();
                System.out.println(line);
            }
        }
    }
}
